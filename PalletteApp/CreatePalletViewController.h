//
//  CreatePalletViewController.h
//  PalletApp
//
//  Created by Celumula,Shiva Teja on 6/18/14.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreatePalletViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *itemListTableView;

- (IBAction)completeButtonAction:(UIButton *)sender;

@end
