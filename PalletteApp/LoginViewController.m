//
//  ViewController.m
//  PalletteApp
//
//  Created by Celumula,Shiva Teja on 6/16/14.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "LoginViewController.h"
#import "HomeViewController.h"
#import "Constants.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Initializing the arrays
    
    palletListArray = [[NSMutableArray alloc] init];
    palletDetailArray = [[NSMutableArray alloc] init];
    palletNameArray = [[NSMutableArray alloc] init];
    itemListArray = [[NSMutableArray alloc] init];    
    
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)LoginButtonAction:(UIButton *)sender {
    
    if ([[_companyNameTF.text lowercaseString] isEqualToString:[_empIdTF.text lowercaseString]] && ![_companyNameTF.text isEqualToString:@""] && ![_empIdTF.text isEqualToString:@""]) {
        
        empFName = _empIdTF.text;
        empID = _empIdTF.text;
        compName = _companyNameTF.text;
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"PalletApp" bundle:nil];
        
        HomeViewController *homeVC = (HomeViewController*)[storyBoard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        
        homeVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        
        [self presentViewController:homeVC animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Invalid Company Name / Employee ID" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }

    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_companyNameTF resignFirstResponder];
    [_empIdTF resignFirstResponder];
    
    return YES;
}

@end
