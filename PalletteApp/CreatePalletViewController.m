//
//  CreatePalletViewController.m
//  PalletApp
//
//  Created by Celumula,Shiva Teja on 6/18/14.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "CreatePalletViewController.h"
#import "Constants.h"

@interface CreatePalletViewController ()

@end

@implementation CreatePalletViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [_itemListTableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [itemListArray count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    
    cell.textLabel.text = [itemListArray objectAtIndex:[indexPath row]];
    cell.textLabel.font  = [UIFont fontWithName:@"Gill Sans" size:12.0];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    return cell;
}

- (IBAction)completeButtonAction:(UIButton *)sender {
    
    [palletNameArray addObject:[NSString stringWithFormat:@"Pallet 00%d",palletCount]];
    [palletDetailArray addObject:itemListArray];
    itemListArray = [[NSMutableArray alloc] init];
    palletCount++;
    
}


@end
