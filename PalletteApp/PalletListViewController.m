//
//  PalletteListViewController.m
//  PalletteApp
//
//  Created by Celumula,Shiva Teja on 6/16/14.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "PalletListViewController.h"
#import "Constants.h"

@interface PalletListViewController ()

@end

@implementation PalletListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
//    palletteListArray = [NSMutableArray arrayWithArray:@[@"Pallette 001",@"Pallette 002",@"Pallette 003",@"Pallette 004",@"Pallette 005",@"Pallette 006",@"Pallette 007",@"Pallette 008"]];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [_palletListTableView reloadData];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [palletNameArray count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    
    palletIndex = indexPath.row;
    
    cell.textLabel.text = [palletNameArray objectAtIndex:[indexPath row]];
    cell.textLabel.font  = [UIFont fontWithName:@"Gill Sans" size:12.0];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    return cell;
}

@end
