//
//  ViewController.h
//  PalletteApp
//
//  Created by Celumula,Shiva Teja on 6/16/14.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *companyNameTF;
@property (strong, nonatomic) IBOutlet UITextField *empIdTF;

- (IBAction)LoginButtonAction:(UIButton *)sender;

@end
