//
//  Constants.m
//  PalletApp
//
//  Created by Celumula,Shiva Teja on 6/18/14.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "Constants.h"

int palletIndex = 0;
int palletCount = 1;

NSString* compName = @"";
NSString* empID = @"";
NSString* empFName = @"";
NSString* empLName = @"";

NSMutableArray *palletNameArray = nil;
NSMutableArray *palletListArray = nil;
NSMutableArray *palletDetailArray = nil;
NSMutableArray *itemListArray = nil;