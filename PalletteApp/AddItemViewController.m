//
//  AddItemViewController.m
//  PalletApp
//
//  Created by Celumula,Shiva Teja on 6/18/14.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "AddItemViewController.h"
#import "Constants.h"

@interface AddItemViewController ()

@end

@implementation AddItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addItemButtonAction:(UIButton *)sender {
    
    if ([_itemNameTF.text isEqualToString:@""] || [_itemQuantTF.text isEqualToString:@""]) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter the Item / Quantity" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        [itemListArray addObject:[NSString stringWithFormat:@"%@ x %@",_itemNameTF.text,_itemQuantTF.text]];
        
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"PalletApp" bundle:nil];
        
        CreatePalletViewController *createPVC = (CreatePalletViewController*)[storyBoard instantiateViewControllerWithIdentifier:@"CreatePalletViewController"];
        
        createPVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        
        [self presentViewController:createPVC animated:YES completion:nil];
        
     
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [_itemNameTF resignFirstResponder];
    [_itemQuantTF resignFirstResponder];
    
    return YES;
}

@end
