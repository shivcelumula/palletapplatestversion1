//
//  AppDelegate.h
//  PalletteApp
//
//  Created by Celumula,Shiva Teja on 6/16/14.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PalletAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
