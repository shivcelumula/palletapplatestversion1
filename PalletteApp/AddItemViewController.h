//
//  AddItemViewController.h
//  PalletApp
//
//  Created by Celumula,Shiva Teja on 6/18/14.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CreatePalletViewController.h"

@interface AddItemViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *itemNameTF;
@property (strong, nonatomic) IBOutlet UITextField *itemQuantTF;

- (IBAction)addItemButtonAction:(UIButton *)sender;

@end
