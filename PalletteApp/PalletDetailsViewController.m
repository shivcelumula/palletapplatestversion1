//
//  PalleteDetailsViewController.m
//  PalletteApp
//
//  Created by Celumula,Shiva Teja on 6/16/14.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import "PalletDetailsViewController.h"
#import "Constants.h"

@interface PalletDetailsViewController ()

@end

@implementation PalletDetailsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
//     palletDetailArray = [NSMutableArray arrayWithArray:@[@"Kiwi x 10",@"Mango x 20",@"Apples x 25",@"Strawberries x 15"]];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [_palletDetailsTableView reloadData];
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[palletDetailArray objectAtIndex:palletIndex] count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier ];
    
    cell.textLabel.text = [[palletDetailArray objectAtIndex:palletIndex] objectAtIndex:[indexPath row]];
    cell.textLabel.font  = [UIFont fontWithName:@"Gill Sans" size:12.0];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    return cell;
}
@end
