//
//  HomeViewController.h
//  PalletteApp
//
//  Created by Celumula,Shiva Teja on 6/16/14.
//  Copyright (c) 2014 Student. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *empFNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *empLNameLbl;

@end
